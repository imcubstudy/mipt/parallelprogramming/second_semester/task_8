// Task 8:
//     - Write a multi-threaded program that will do QuckSort of an array with size up to 10'000
//     - implement input of an array via file
//     - implement input of an array via keyboard
//     - be aware of border conditions
//     - bonus:
//         - do not create a copy of the array, modify original one
// Comment: bonus task is done!
// Author: Gazizov Yakov

#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <string>
#include <string_view>
#include <exception>
#include <algorithm>
#include <limits>
#include <random>
#include <omp.h>

using namespace std::literals;

// abstract away random int generation
class Random {
public:
    static std::int32_t getInt32()
    {
        return m_distribution(m_gen);
    }

private:
    static std::default_random_engine m_gen;
    static std::uniform_int_distribution<std::int32_t> m_distribution;
};
auto Random::m_gen = std::default_random_engine(std::random_device()());
auto Random::m_distribution = std::uniform_int_distribution<std::int32_t>(std::numeric_limits<std::int32_t>::min(),
    std::numeric_limits<std::int32_t>::max());

// just a helper class to parse CLI
struct ProgramParams {
public:
    enum class Param : std::uint8_t {
        INPUT_MODE,
        NUM_ELEMENTS,
        FILENAME,
        MAX_PARAM
    };

    Param ParamFromSV(std::string_view paramSV)
    {
        if (paramSV == "--mode"sv) {
            return Param::INPUT_MODE;
        } else if (paramSV == "--num_elements"sv) {
            return Param::NUM_ELEMENTS;
        } else if (paramSV == "--filename"sv) {
            return Param::FILENAME;
        } else {
            throw std::runtime_error("Unexpected param "s + std::string(paramSV) + "\n"
                "Supported params are: mode, num_elements, filename"s
            );
        }
    };

    enum class Mode : std::uint8_t {
        RANDOM,
        FILE,
        KEYBOARD,
        MAX_MODE
    };

    Mode ModeFromSV(std::string_view modeSV)
    {
        if (modeSV == "random"sv) {
            return Mode::RANDOM;
        } else if (modeSV == "file"sv) {
            return Mode::FILE;
        } else if (modeSV == "keyboard"sv) {
            return Mode::KEYBOARD;
        } else {
            throw std::runtime_error("Unexpected mode "s + std::string(modeSV) + "\n"
                "Supported modes are: random, file, keyboard"s
            );
        }
    };


    ProgramParams(int const argc, char const *const argv[])
    {
        auto mode = Mode::MAX_MODE;
        auto numElements = 0U;
        auto fileName = ""s;

        if (argc == 1) {
            throw std::runtime_error("Usage: ./run.sh --mode <mode> --num_elements <num_elements> [--filename <filename>]"s);
        }

        for (int arg = 1; arg < argc; arg += 2) {
            auto const param = ParamFromSV(argv[arg]);
            if (arg + 1 == argc) {
                throw std::runtime_error("Argument not supplied for param "s + std::string(argv[arg]));
            }

            switch (param)
            {
                case Param::INPUT_MODE: {
                    mode = ModeFromSV(argv[arg + 1]);
                    break;
                }
                case Param::NUM_ELEMENTS: {
                    if (auto ret = std::sscanf(argv[arg + 1], "%u", &numElements);
                        !(ret != EOF && ret == 1) || numElements < 1) {
                        throw std::runtime_error("Invalid value for num_elements");
                    }
                    break;
                }
                case Param::FILENAME: {
                    fileName = argv[arg + 1];
                    break;
                }
                default: {
                    break;
                }
            }
        }

        if (mode == Mode::FILE && fileName.empty()) {
            throw std::runtime_error("file mode chosen, but filename is not supplied");
        }

        array.resize(numElements);

        switch (mode)
        {
            case Mode::RANDOM: {
                for (auto i = 0; i < numElements; ++i) {
                    array[i] = Random::getInt32();
                }
                break;
            }
            case Mode::FILE: {
                auto handle = std::fopen(fileName.c_str(), "r");
                if (!handle) {
                    throw std::runtime_error("Failed to open file "s + fileName);
                }

                for (auto i = 0; i < numElements; ++i) {
                    if (auto ret = std::fscanf(handle, "%d", &array[i]); !(ret != EOF && ret == 1)) {
                        throw std::runtime_error("Failed to read data from file "s + fileName);
                    }
                }
                break;
            }
            case Mode::KEYBOARD: {
                for (auto i = 0; i < numElements; ++i) {
                    if (auto ret = std::scanf("%d", &array[i]); !(ret != EOF && ret == 1)) {
                        throw std::runtime_error("Failed to read data from keyboard");
                    }
                }
                break;
            }
            default: {
                break;
            }
        }
    }

    std::vector<std::int32_t> array = {};
};

// well, quickSort
void quickSort(std::int32_t *array, std::int32_t n)
{
    // do the job in this thread
    std::int32_t i = 0, j = n;
    std::int32_t pivot = array[n / 2];

    do {
        while (array[i] < pivot) {
            i++;
        }
        while (array[j] > pivot) {
            j--;
        }

        if (i <= j) {
            std::swap(array[i], array[j]);
            i++; j--;
        }
    } while (i <= j);

    // do not use mutli-threading if array is smol
    constexpr std::uint32_t SIZE_THRESHOLD = 64;
    if (n < SIZE_THRESHOLD) {
        if (j > 0) {
            quickSort(array, j);
        }
        if (n > i) {
            quickSort(array + i, n - i);
        }
        return;
    }

    // otherwise enqueue tasks to sort each portion of the array
    #pragma omp task shared(array)
    {
        if (j > 0) {
            quickSort(array, j);
        }
    }
    #pragma omp task shared(array)
    {
        if (n > i) {
            quickSort(array + i, n - i);
        }
    }

    // wait to abovementioned tasks to finish
    #pragma omp taskwait
}

int main(int const argc, char const *const argv[])
{
    // Get array from whatever source based on CLI
    auto array = std::vector<std::int32_t>{};
    try {
        array = ProgramParams(argc, argv).array;
    } catch (std::exception const &e) {
        std::printf("%s\n", e.what());
    }

    // Generate ground truth to check ourselves later
    auto groundTruth = array;
    std::sort(std::begin(groundTruth), std::end(groundTruth));

    // we do need to be able to enqueue tasks
    #pragma omp parallel default(shared)
    {
        // but we need to sort this array only once and avoid data races
        #pragma omp single nowait
        {
            quickSort(array.data(), static_cast<std::int32_t>(std::size(array)) - 1);
        }
    }

    // compare our result to the ground truth
    bool same = true;
    for (std::uint32_t i = 0; i < array.size() && same; ++i) {
        if (array[i] != groundTruth[i]) {
            same = false;
        }
    }

    // praise the sun if everything went as planned
    if (same) {
        std::printf("Sorted succesfully!\n");
    } else {
        std::printf("Ooopsie...\n");
    }

    return 0;
}
